/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';

var User = require('../api/user/user.model');

User.find({ email: 'superadmin' }, function (err, users) {
    if (err) {
        console.log(err);
        return;
    }
    console.log(users.length);
    if (!users.length) {
        User.create({
            provider: 'local',
            role: 'superadmin',
            name: 'SuperAdmin',
            email: 'superadmin',
            password: 'superadmin'
        }, function () {
            console.log('inserted super admin');
        })
    }
});