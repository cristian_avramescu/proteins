'use strict';

// Development specific configuration
// ==================================
module.exports = {
  // MongoDB connection options
  mongo: {
    uri: 'mongodb://admin:w0rk1ngH4rd@ds147487.mlab.com:47487/vigruls-database'
  },

  seedDB: true
};
