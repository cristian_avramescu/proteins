/**
 * Main application routes
 */

'use strict';

module.exports = function (app) {
  // Insert routes below
  app.use('/api/box', require('./api/box'));
  app.use('/api/proteins', require('./api/protein'));
  app.use('/api/users', require('./api/user'));
  app.use('/api/seed', require('./api/seed'));

  app.use('/auth', require('./auth'));

  // All undefined asset or api routes should return a 404
  app.route('/:url(api|auth|components|app|bower_components|assets)/*')
    .get(function (req, res) {
      res.status(404).send({ message: 'Path not found' });
    });

  // All other routes should redirect to the index.html
  app.route('/*')
    .get(function (req, res) {
      res.sendFile(app.get('appPath') + '/index.html');
    });
};
