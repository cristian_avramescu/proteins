'use strict';

var BoxService = require('./box.service');

exports.index = index;
exports.show = show;
exports.create = create;
exports.update = update;
exports.destroy = destroy;

// Get list of box
function index(req, res) {
  var filter = {};
  console.log(req.query.name);
  if(req.query.name) {
    var namesFilters = [];
    var names = req.query.name.split('|');
    for (var i = 0; i < names.length; i++) {
      namesFilters.push({
        name: names[i]
      });
    }
    filter['$or'] = namesFilters;
  }
  
  BoxService
    .index(filter)

    .then(function(boxes) {
      res.status(200).json(boxes);
    })
    .catch(function(err) {
      res.status(500).send(err);
    });
};

// Get a single box
function show(req, res) {
  BoxService
    .show(req.params.id)

    .then(function(box) {
      res.json(box);
    })
    .catch(function(err) {
      if(err.code === 'NOT_FOUND') {
        return res.sendStatus(404);
      } 
      res.status(500).send(err);
    });
};

// Creates a new box in the DB.
function create(req, res) {
  BoxService
    .create(req.body)

    .then(function(box) {
      res.status(201).json(box);
    })
    .catch(function(err) {
      res.status(500).send(err);
    });
};

// Updates an existing box in the DB.
function update(req, res) {
  if(req.body._id) { delete req.body._id; }

  BoxService
    .update(req.params.id, req.body)

    .then(function(box) {
      res.status(201).json(box);
    })
    .catch(function(err) {
      if(err.code === 'NOT_FOUND') {
        return res.sendStatus(404);
      } 
      res.status(500).send(err);
    });
};

// Deletes a box from the DB.
function destroy(req, res) {
  BoxService
    .destroy(req.params.id)

    .then(function(box) {
      res.sendStatus(204);
    })
    .catch(function(err) {
      if(err.code === 'NOT_FOUND') {
        return res.sendStatus(404);
      } 
      res.status(500).send(err);
    });
};
