'use strict';

var Box = require('./box.model');
var _ = require('lodash');

function getBestStartingPoint(availableBoxes, boxSize, nb) {
    var min = boxSize + 1;
    var bestIndex = -1;
    var bestBox = null;
    for (var i = 0; i < availableBoxes.length; i++) {
        var availableBox = availableBoxes[i];
        for (var key in availableBox.availableSpaces) {
            if (availableBox.availableSpaces.hasOwnProperty(key) && key !== 'size') {
                var element = availableBox.availableSpaces[key];
                if ((element - nb) < min) {
                    min = (element - nb);
                    bestIndex = key;
                    bestBox = availableBox.box;
                }
            }
        }
    }
    return {
        box: bestBox,
        startingIndex: Number(bestIndex)
    };
}

function getAllAvailableSpacesForBox(tubes, nb) {
    var availableSpaces = { size: 0 };
    var consecutiveEmptySpaces = 0;
    var startingIndex = -1;
    for (var i = 0; i < tubes.length; i++) {
        var tube = tubes[i];
        if (tube.lot === null) {
            if (consecutiveEmptySpaces === 0) {
                startingIndex = i;
            }
            consecutiveEmptySpaces++;
        } else {
            if (consecutiveEmptySpaces && consecutiveEmptySpaces >= nb) {
                availableSpaces[startingIndex] = consecutiveEmptySpaces;
                availableSpaces.size++;
            }
            consecutiveEmptySpaces = 0;
            startingIndex = -1;
        }
    }
    if (consecutiveEmptySpaces && consecutiveEmptySpaces >= nb) {
        availableSpaces[startingIndex] = consecutiveEmptySpaces;
        availableSpaces.size++;
    }
    return availableSpaces;
}

function getNextAvailableNameIndex(boxList, numberOfCreatedBoxes) {
    var maxBox = 0;
    if (boxList.length !== 0) {
        for (var i = 0; i < boxList.length; i++) {
            var box = boxList[i];
            var name = box.name;
            var indexStr = name.substring(name.lastIndexOf(' '));
            var index = Number(indexStr);
            if (!isNaN(index) && index > maxBox) {
                maxBox = index;
            }
        }
    }
    return maxBox + 1 + numberOfCreatedBoxes;
}

function getEmptyTubes(size) {
    var tubes = [];
    for (var i = 0; i < size; i++) {
        tubes.push({
            protein: null,
            lot: null,
            volume: null
        });
    }
    return tubes;
}

function getAllAvailableSpaces(boxList, conditions, nbTubes) {
    var availableSpacesByBox = [];
    for (var i = 0; i < boxList.length; i++) {
        var box = boxList[i];
        if (box.temperature === conditions.temperature &&
            box.size === conditions.size) {
            var availableSpaces = getAllAvailableSpacesForBox(box.tubes, nbTubes);
            if (availableSpaces.size) {
                availableSpacesByBox.push({
                    box: box,
                    availableSpaces: availableSpaces
                });
            }
        }
    }
    return availableSpacesByBox;
}

function createNewBox(boxList, informations, newBoxes) {
    return new Box({
        drawer: informations.project,
        project: informations.project,
        name: informations.project + ' ' + getNextAvailableNameIndex(boxList, newBoxes),
        temperature: informations.temperature,
        size: informations.size,
        tubes: getEmptyTubes(informations.size),
        _id: null
    });
}

function fill(boxTubes, tubes, startingIndex) {
    var endIndex = Math.min(boxTubes.length, tubes.length);
    for (var i = 0; i < endIndex; i++) {
        var tube = tubes[0];
        var boxTube = boxTubes[startingIndex + i];
        _.assign(boxTube, tube);
        tubes.splice(0, 1);
    }
}

function fillOneBox(boxList, infos, tubes) {
    var availableSpaces = getAllAvailableSpaces(boxList, infos, tubes.length);
    var bestBoxInfo = getBestStartingPoint(availableSpaces, infos.size, tubes.length);
    if (!bestBoxInfo.box) {
        bestBoxInfo.box = createNewBox(boxList, infos, infos.newBoxes);
        bestBoxInfo.startingIndex = 0;
        infos.newBoxes++;
    }
    fill(bestBoxInfo.box.tubes, tubes, bestBoxInfo.startingIndex);
    return bestBoxInfo.box;
}

function getBoxSize(tubeDef) {
    var volume = tubeDef.volume;
    if (tubeDef.unit === 'µL') {
        volume = volume / 1000;
    }
    if (volume > 1) {
        return 16;
    }
    return 64;
}

function getListsOfTubes(protein) {
    var resultingTubes = {};
    for (var i = 0; i < protein.tubes.length; i++) {
        var tubeDefinition = protein.tubes[i];
        if(tubeDefinition.placed && !tubeDefinition.unsaved) {
            continue;
        }
        var boxSize = getBoxSize(tubeDefinition);
        if (!resultingTubes[boxSize]) {
            resultingTubes[boxSize] = [];
        }
        var tubes = resultingTubes[boxSize];
        for (var j = 0; j < tubeDefinition.number; j++) {
            tubes.push({
                lot: protein.lot,
                protein: protein.name,
                concentration: protein.concentration,
                volume: tubeDefinition.volume,
                unit: tubeDefinition.unit
            })
        }
    }
    return resultingTubes;
}

function fillWithTubes(protein, res) {
    var filter = {
        project: protein.project
    };
    var tubesListBySize = getListsOfTubes(protein);
    var infos = {
        project: protein.project,
        temperature: protein.temperature,
        newBoxes: 0
    };

    Box.find(filter, function (err, boxList) {
        if (err) {
            console.log(err);
            res.status(500).send(err);
        } else {
            var resultList = [];
            for (var size in tubesListBySize) {
                if (tubesListBySize.hasOwnProperty(size)) {
                    var tubes = tubesListBySize[size];
                    infos.size = Number(size);
                    while (tubes.length) {
                        resultList.push(fillOneBox(boxList, infos, tubes));
                    }
                }
            }
            var result = {
                list: resultList,
                total: resultList.length
            };
            res.status(200).json(result);
        }
    });
}

function removeProteinLotFromBox(protein, lot, boxNames, deferred) {
    if(!boxNames) {
        return deferred.resolve(204);
    }
    boxNames = boxNames.split('|');
    var namesFilters = [];
    for (var i = 0; i < boxNames.length; i++) {
        namesFilters.push({ name: boxNames[i] });
    }
    var filter = { $or: namesFilters };
    Box.find(filter, function (err, boxList) {
        if(err) {
            return deferred.reject(err);
        }
        var total = boxList.length;
        if(!total) {
            return deferred.resolve(204);
        }
        for (var i = 0; i < boxList.length; i++) {
            var box = boxList[i];
            for (var j = 0; j < box.tubes.length; j++) {
                var boxTube = box.tubes[j];
                if(boxTube.protein === protein && boxTube.lot === lot) {
                    boxTube.protein = null;
                    boxTube.lot = null;
                    boxTube.concentration = null;
                    boxTube.volume = null;
                    boxTube.unit = null;
                }
            }
            box.save(function(err, box) {
                if(err) {
                    return deferred.reject(err);
                }
                console.log('box ' + i + ' out of ' + boxList.length + 'saved');
                if(!--total) {
                    deferred.resolve(204);
                }
            });
        }
    });
}

exports.fillBox = fillWithTubes;
exports.removeProtein = removeProteinLotFromBox;
