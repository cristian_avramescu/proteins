'use strict';

var _ = require('lodash');
var Q = require('q');
var Box = require('./box.model');

exports.index = index;
exports.show = show;
exports.create = create;
exports.update = update;
exports.destroy = destroy;

// Get list of box
function index(filter) {
  var deferred = Q.defer();

  Box.find(filter, function (err, boxes) {
    if(err) {
      return deferred.reject(err);
    }
    var result = {
      list: boxes,
      total: boxes.length
    };
    return deferred.resolve(result);
  });
  return deferred.promise;
};

// Get a single box
function show(id) {
  var deferred = Q.defer();

  Box.findById(id, function (err, box) {
    if(err) return deferred.reject(err);
    if (!box) return deferred.reject(
      Error.new({
        code: 'NOT_FOUND',
        message: 'Box: ' + id + ' is not found.'
      })
    );
    deferred.resolve(box);
  });
  return deferred.promise;
};

// Creates a new box in the DB.
function create(params) {
  var deferred = Q.defer();

  Box.create(params, function (err, box) {
    if(err) return deferred.reject(err);
    deferred.resolve(box);
  });
  return deferred.promise;
};

// Updates an existing box in the DB.
function update(id, params) {
  var deferred = Q.defer();

  Box.findById(id, function (err, box) {
    if(err) return deferred.reject(err);
    if (!box) return deferred.reject(
      Error.new({
        code: 'NOT_FOUND',
        message: 'Box: ' + id + ' is not found.'
      })
    );

    var updated = _.merge(box, params);
    if (params.tubes instanceof Array) {
     updated.tubes = params.tubes;
    }
    updated.save(function (err) {
      if (err) { return deferred.reject(err); }
      return deferred.resolve(box);
    }); 
  });
  return deferred.promise;
};

// Deletes a box from the DB.
function destroy(id) {
  var deferred = Q.defer();

  Box.findById(id, function (err, box) {
    if(err) return deferred.reject(err);
    if (!box) return deferred.reject(
      Error.new({
        code: 'NOT_FOUND',
        message: 'Box: ' + id + ' is not found.'
      })
    );

    box.remove(function(err) {
      if(err) { return deferred.reject(err); }
      return deferred.resolve(204);
    });
    
  });
  return deferred.promise;
};
