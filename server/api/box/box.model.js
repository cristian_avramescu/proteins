'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var BoxSchema = new Schema({
  name: String,
  project: String,
  drawer: String,
  temperature: Number,
  size: Number,
  tubes: [
    {
      protein: String,
      lot: String,
      volume: Number,
      unit: String,
      concentration: Number
    }
  ]
});

module.exports = mongoose.model('Box', BoxSchema);