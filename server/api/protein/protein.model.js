'use strict';

var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

var Schema = mongoose.Schema;

var ProteinSchema = new Schema({
    lot: {
        type: String,
        required: true,
        index: true
    },
    name: {
        type: String,
        required: true
    },
    project: {
        type: String,
        required: true
    },
    creationDate: {
        type: Date,
        required: false
    },
    finishedDate: Date,
    temperature: {
        type: Number,
        required: false
    },
    fullname: String,
    concentration: Number,
    initialVolume: String,
    initialQuantity: String,
    initialTubesNb: Number,
    tubes: [
        {
            volume: Number,
            unit: String,
            number: Number,
            placed: Boolean
        }
    ],
    box: String,
    comments: String,
    projectToLower: String,
    nameToLower: String,
    editMode: Boolean,
    files: [
        {
            name: String,
            creationDate: Date,
            path: String,
            version: Number
        }
    ],
    history: [
        {
            date: Date,
            author: String,
            message: [String]
        }
    ]
});

ProteinSchema.index({ name: 'text', project: 'text', lot: 'text', fullname: 'text' });

ProteinSchema.plugin(mongoosePaginate);

ProteinSchema.pre('save', function (next) {
    if (typeof this.name === 'string') {
        this.nameToLower = this.name.toLowerCase();
    }
    if (typeof this.project === 'string') {
        this.projectToLower = this.project.toLowerCase();
    }
    if (!this.creationDate && this.lot.indexOf('201') === 0) {
        var date = new Date();
        var year = this.lot.substr(0, 4);
        var month = this.lot.substr(4, 2);
        var day = this.lot.substr(6, 2);
        date.setFullYear(year);
        date.setMonth(month - 1);
        date.setDate(day);
        this.creationDate = date;
    }
    next();
});

module.exports = mongoose.model('Protein', ProteinSchema);