'use strict';

var ProteinService = require('./protein.service');
var BoxFiller = require('../box/box.filler');
var config = require('../../config/environment');

exports.index = index;
exports.show = show;
exports.create = create;
exports.update = update;
exports.destroy = destroy;
exports.addToBox = addToBox;
exports.fileUpload = fileUpload;
exports.fileDownload = fileDownload;
exports.fileDelete = fileDelete;

function parseOptions(req) {
    var params = req.query;
    var options = {
        //by default, sort by creation date ASC
        creationDate: 1
    };
    //by default, get all proteins
    var filter = {};
    var numericLimit = Number(params.limit);
    if (params.limit && !isNaN(numericLimit)) {
        options.limit = numericLimit;
    }
    if (params.page && params.page.length > 0) {
        options.page = params.page;
    }
    if (params.order && params.order.length > 0) {
        var order = params.order;
        var direction = 1;
        if (order.charAt(0) === '-') {
            order = order.substring(1);
            direction = -1;
        }
        options.sort = {};
        options.sort[order] = direction;
    }
    if (params.filter && params.filter.length > 0) {
        var textSearch = {$text: { $search: params.filter }}
        if(params.filter.indexOf(' ') === -1) {
            filter.$or = [
                textSearch,
                {lot: {$regex: params.filter, $options: 'i'}}
            ];
        } else {
            filter = textSearch;
        }
    }
    return {
        options: options,
        filter: filter
    };
}

// Get list of protein
function index(req, res) {
    ProteinService
        .index(parseOptions(req))

        .then(function (proteins) {
            res.status(200).json(proteins);
        })
        .catch(function (err) {
            res.status(500).send(err);
        });
};

// Get a single protein
function show(req, res) {
    ProteinService
        .show(req.params.id)

        .then(function (protein) {
            res.json(protein);
        })
        .catch(function (err) {
            if (err.code === 'NOT_FOUND') {
                return res.status(404).send(err);
            }
            if (err.name === 'CastError') {
                return res.status(400).send(err);
            }
            res.status(500).send(err);
        });
};

// Creates a new protein in the DB.
function create(req, res) {
    ProteinService
        .create(req.body)

        .then(function (protein) {
            res.status(201).json(protein);
        })
        .catch(function (err) {
            if (err.name === 'ValidationError') {
                res.status(400).json(err);
            } else {
                res.status(500).send(err);
            }
        });
};

// Updates an existing protein in the DB.
function update(req, res) {
    if (req.body._id) { delete req.body._id; }

    ProteinService
        .update(req.params.id, req.body)
        .then(function (protein) {
            res.status(201).json(protein);
        })
        .catch(function (err) {
            if (err.code === 'NOT_FOUND') {
                return res.status(404).send();
            }
            res.status(500).send(err);
        });
};

function addToBox(req, res) {
    BoxFiller.fillBox(req.body, res);
}

// Deletes a protein from the DB.
function destroy(req, res) {
    ProteinService
        .destroy(req.params.id)

        .then(function (protein) {
            res.sendStatus(204);
        })
        .catch(function (err) {
            if (err.code === 'NOT_FOUND') {
                return res.sendStatus(404);
            }
            res.status(500).send(err);
        });
};

function fileUpload(req, res) {
    ProteinService
        .fileUpload(req, res)
        .then(function (protein) {
            res.status(200).send(protein);
        })
        .catch(function (err) {
            if (err.code === 'NOT_FOUND') {
                return res.status(404).send(err);
            }
            res.status(500).send(err);
        });
}

function fileDownload(req, res) {
    ProteinService
        .fileDownload(req.params.id, req.params.fileId)
        .then(function (result) {
            var options = {
                dotfiles: 'deny',
                headers: {
                    'x-timestamp': Date.now(),
                    'x-sent': true
                }
            };
            res.append('Content-Disposition', 'inline');
            res.sendFile(result.path, function(err) {
                if(err) {
                    console.log(err);
                }
                console.log('file sent');
            });
        })
        .catch(function (err) {
            if (err.code === 'NOT_FOUND') {
                return res.status(404).send(err);
            }
            if (err.code === 'GONE') {
                return res.status(410).send(err);
            }
            res.status(500).send(err);
        });
}

function fileDelete(req, res) {
    ProteinService
        .fileDelete(req.params.id, req.params.fileId)
        .then(function (protein) {
            res.status(200).send(protein);
        })
        .catch(function (err) {
            if (err.code === 'NOT_FOUND') {
                return res.status(404).send(err);
            }
            res.status(500).send(err);
        });
}
