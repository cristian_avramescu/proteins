'use strict';

var express = require('express');
var controller = require('./protein.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/', auth.hasRole('guest'), controller.index);
router.get('/:id', auth.hasRole('guest'), controller.show);
router.post('/', auth.hasRole('user'), controller.create);
router.post('/addToBox', auth.hasRole('user'), controller.addToBox);
router.put('/:id', auth.hasRole('user'), controller.update);
router.post('/:id', auth.hasRole('user'), controller.update);
router.patch('/:id', auth.hasRole('user'), controller.update);
router.delete('/:id', auth.hasRole('admin'), controller.destroy);
router.post('/:id/file', auth.hasRole('user'), controller.fileUpload);
router.get('/:id/file/:fileId', controller.fileDownload);
router.delete('/:id/file/:fileId', auth.hasRole('user'), controller.fileDelete);

module.exports = router;