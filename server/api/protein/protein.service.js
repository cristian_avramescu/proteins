'use strict';

var _ = require('lodash');
var Q = require('q');
var Protein = require('./protein.model');
var multer = require('multer');
var config = require('../../config/environment');
var boxFiller = require('../box/box.filler');
var mkdirp = require('mkdirp');
var fs = require('fs');
var path = require('path');

function findFileVersion(protein, fileName) {
  var existingVersion = 0;
  for (var i = 0; i < protein.files.length; i++) {
    var file = protein.files[i];
    if (file.name === fileName && file.version > existingVersion) {
      existingVersion = file.version;
    }
  }
  return existingVersion + 1;
}

var storage = multer.diskStorage({
  destination: function (req, file, callback) {
    var proteinName = req.protein.name.replace(/ /g, '_');
    var proteinLot = req.protein.lot.replace(/ /g, '_');
    var project = req.protein.project.replace(/ /g, '_');
    var folder = path.join(config.filesUploadBaseDir, project, proteinName, proteinLot);
    if (!fs.existsSync(folder)) {
      mkdirp.sync(folder);
    }
    callback(null, folder);
  },
  filename: function (req, file, callback) {
    var filename = req.body.filename;
    var formattedFilename = req.body.filename.replace(/ /g, '_');
    var extension = path.extname(file.originalname);
    var version = findFileVersion(req.protein, filename);
    callback(null, formattedFilename + '_V' + version + extension);
  }
});
var upload = multer({ storage: storage }).single('file');

exports.index = index;
exports.show = show;
exports.create = create;
exports.update = update;
exports.destroy = destroy;
exports.fileUpload = fileUpload;
exports.fileDownload = fileDownload;
exports.fileDelete = fileDelete;

// Get list of protein
function index(params) {
  var deferred = Q.defer();
  if (!params) {
    params = {};
  }
  Protein.paginate(params.filter || {}, params.options || {}, function (err, proteins) {
    if (err) return deferred.reject(err);
    deferred.resolve(proteins);
  });
  return deferred.promise;
};

// Get a single protein
function show(id) {
  var deferred = Q.defer();

  Protein.findById(id, function (err, protein) {
    if (err) return deferred.reject(err);
    if (!protein) return deferred.reject(
      {
        code: 'NOT_FOUND',
        message: 'Protein: ' + id + ' is not found.'
      }
    );
    deferred.resolve(protein);
  });
  return deferred.promise;
};

// Creates a new protein in the DB.
function create(params) {
  var deferred = Q.defer();

  Protein.create(params, function (err, protein) {
    if (err) return deferred.reject(err);
    deferred.resolve(protein);
  });
  return deferred.promise;
};

// Updates an existing protein in the DB.
function update(id, params) {
  var deferred = Q.defer();

  Protein.findById(id, function (err, protein) {
    if (err) return deferred.reject(err);
    if (!protein) return deferred.reject(
      {
        code: 'NOT_FOUND',
        message: 'Protein: ' + id + ' is not found.'
      }
    );

    var updated = _.merge(protein, params);
    if (params.tubes instanceof Array) {
      _.remove(updated.tubes, undefined);
      _.merge(updated.tubes, params.tubes);
    }
    if (params.history instanceof Array) {
      _.merge(updated.history, params.history);
    }
    if (params.files instanceof Array) {
      _.merge(updated.files, params.files);
    }
    updated.save(function (err) {
      if (err) { return deferred.reject(err); }
      return deferred.resolve(protein);
    });
  });
  return deferred.promise;
}

// Deletes a protein from the DB.
function destroy(id) {
  var deferred = Q.defer();

  Protein.findById(id, function (err, protein) {
    if (err) return deferred.reject(err);
    if (!protein) return deferred.reject(
      {
        code: 'NOT_FOUND',
        message: 'Protein: ' + id + ' is not found.'
      }
    );

    protein.remove(function (err) {
      if (err) { return deferred.reject(err); }
      boxFiller.removeProtein(protein.name, protein.lot, protein.box, deferred);
    });

  });
  return deferred.promise;
}

function fileUpload(req, res) {
  var deferred = Q.defer();
  Protein.findById(req.params.id, function (err, protein) {
    if (err) return deferred.reject(err);
    if (!protein) return deferred.reject(
      {
        code: 'NOT_FOUND',
        message: 'Protein: ' + req.params.id + ' is not found.'
      }
    );
    req.protein = protein;
    upload(req, res, function (err) {
      if (err) return deferred.reject(err);
      if (!req.file) return deferred.reject('something went wrong');
      var newFileName = req.body.filename;
      protein.files.push({
        name: newFileName,
        path: req.file.path,
        creationDate: new Date(),
        version: findFileVersion(protein, newFileName)
      });
      protein.save(function (err) {
        if (err) { return deferred.reject(err); }
        return deferred.resolve(protein);
      });
    });
  });
  return deferred.promise;
}

function fileDownload(proteinId, fileId) {
  var deferred = Q.defer();
  Protein.findById(proteinId, function (err, protein) {
    if (err) return deferred.reject(err);
    if (!protein) return deferred.reject({
      code: 'NOT_FOUND',
      message: 'Protein: ' + proteinId + ' is not found.'
    }
    );
    var file = null;
    for (var i = 0; i < protein.files.length; i++) {
      var f = protein.files[i];
      if (f._id.equals(fileId)) {
        file = f;
        break;
      }
    }
    if (!file) return deferred.reject({
      code: 'NOT_FOUND',
      message: 'File not found.'
    });
    var filePath;
    if(file.creationDate) {
      filePath = file.path;
    } else {
      filePath = path.join(config.filesUploadBaseDir, file.path);
    }
    if (!fs.existsSync(filePath)) return deferred.reject({
      code: 'GONE',
      message: 'The file does not exist'
    });
    deferred.resolve({ path: filePath, filename: path.basename(filePath) });
  });
  return deferred.promise;
}

function fileDelete(proteinId, fileId) {
  var deferred = Q.defer();
  Protein.findById(proteinId, function (err, protein) {
    if (err) return deferred.reject(err);
    if (!protein) return deferred.reject({
      code: 'NOT_FOUND',
      message: 'Protein: ' + proteinId + ' is not found.'
    });
    var file = null;
    var fileIndex = 0;
    for (fileIndex; fileIndex < protein.files.length; fileIndex++) {
      var f = protein.files[fileIndex];
      if (f._id.equals(fileId)) {
        file = f;
        break;
      }
    }
    if (!file) return deferred.reject({
      code: 'NOT_FOUND',
      message: 'File not found.'
    });
    if (fs.existsSync(file.path)) {
      fs.unlinkSync(file.path);
    }
    protein.files.splice(fileIndex, 1);
    protein.save(function (err) {
      if (err) {
        return deferred.reject(err);
      }
      deferred.resolve(protein);
    })
  });
  return deferred.promise;
}