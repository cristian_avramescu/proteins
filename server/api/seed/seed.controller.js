'use strict';

var config = require('../../config/environment');
var path = require('path');
var multer = require('multer');
var upload = multer({ dest: path.join(config.root, 'temp') }).single('file');
var docParser = require('./docParser');
var Box = require('../box/box.model');
var Protein = require('../protein/protein.model');
var fs = require('fs');

exports.upload = docUpload;

function docUpload(req, res) {
    upload(req, res, function (err) {
        if (err) return res.status(500).send(err);
        if (!req.file) return res.status(500).send('Something went wrong');
        var docs = docParser.parse(req.file.path);
        if (docs.proteins && docs.boxes) {
            Protein.find({}).remove(function () {
                Protein.create(docs.proteins, function (err) {
                    if (err) return res.status(500).send(err);
                    Box.find({}).remove(function () {
                        Box.create(docs.boxes, function(err) {
                            if(err) return res.status(500).send(err);
                            fs.unlinkSync(req.file.path);
                            res.status(200).send({
                                proteins: docs.proteins.length,
                                boxes: docs.boxes.length
                            });
                        });
                    });
                });
            });
        } else {
            res.sendStatus(400);
        }
    });
}
