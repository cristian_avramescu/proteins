function getValue(sheet, address, type, formatted) {
    var cell = sheet[address];
    var value = null;
    if(cell) {
        if(type === undefined || type === String) {
            value = cell.w;
            if(formatted) {
                value = value.replace('.', ',');
            }
        } else if(type === Number) {
            value = cell.v;
        }
    }
    return value;
}

module.exports.getValue = getValue;