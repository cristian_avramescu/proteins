var tools = require('./tools.js');

function getFile(sheet, address) {
    var cellInfo = sheet[address];
    if (cellInfo) {
        switch (cellInfo.w) {
            case 'RF':
                name = 'Release form';
                break;
            case 'FTP':
                name = 'Fiche technique protéine';
                break;
            case 'CoA':
                name = "Certificat d'analyse";
                break;
            case 'CoAMS':
            case 'CoA_MS':
            case 'CoA MS':
                name = "Certificat d'analyse (méthodes supplémentaires)";
                break;
            case 'MS':
                name = "Analyse spectrométrie de masse";
                break;
            default:
                return null;
        }
        if (cellInfo.l) {
            var path = decodeURIComponent(cellInfo.l.Target);
            return {
                name: name,
                version: 1,
                path: path
            };
        }
    }
    return null;
}

function addFile(protein, worksheet, address) {
    var file = getFile(worksheet, address);
    if (file) {
        protein.files.push(file);
    }
}

function parse(worksheet, startIndex, endIndex) {

    var proteins = [];

    for (var i = startIndex; i <= endIndex; i++) {
        var protein = {};
        protein.tubes = [];
        protein.project = tools.getValue(worksheet, 'A' + i);
        protein.name = tools.getValue(worksheet, 'B' + i);
        protein.lot = tools.getValue(worksheet, 'C' + i);
        protein.fullname = tools.getValue(worksheet, 'D' + i);
        protein.concentration = tools.getValue(worksheet, 'E' + i, Number);
        protein.initialVolume = tools.getValue(worksheet, 'F' + i, String, true);
        protein.initialVolume_numeric = tools.getValue(worksheet, 'F' + i, Number);
        protein.initialQuantity = tools.getValue(worksheet, 'G' + i, String, true);
        protein.initialQuantity_numeric = tools.getValue(worksheet, 'G' + i, Number);
        protein.initialTubesNb = tools.getValue(worksheet, 'H' + i, Number);
        protein.box = tools.getValue(worksheet, 'M' + i);
        if (protein.box) {
            protein.box = protein.box.replace(/( \| )/g, '|');
        }
        if (!protein.box || protein.box === '-') {
            protein.box = '';
            protein.finishedDate = new Date(2007, 6, 7);
        }
        protein.files = [];
        addFile(protein, worksheet, 'I' + i);
        addFile(protein, worksheet, 'J' + i);
        addFile(protein, worksheet, 'K' + i);
        addFile(protein, worksheet, 'O' + i);

        var suiviDuStock = tools.getValue(worksheet, 'L' + i);
        var remarques = tools.getValue(worksheet, 'N' + i);
        var comments = '';
        if (suiviDuStock) {
            comments += suiviDuStock;
        }
        if (remarques) {
            if (comments !== '') {
                comments += '\n';
            }
            comments += remarques;
        }
        protein.comments = comments;

        proteins.push(protein);
    }

    return proteins;
}

module.exports.parse = parse;