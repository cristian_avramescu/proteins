var xlsx = require('xlsx');
// var jsonfile = require('jsonfile');
var proteinsParser = require('./proteinsParser.js');
var boxParser = require('./boxParser.js');
var tools = require('./tools.js');

function compareProteins(protein1, protein2) {
    var protein1Lot = protein1.lot || '';
    var protein2Lot = protein2.lot || '';
    if (protein1.lot && protein1.lot.indexOf(' ') > 0) {
        protein1Lot = protein1.lot.substring(0, protein1.lot.indexOf(' '));
    }
    if (protein2.lot && protein2.lot.indexOf(' ') > 0) {
        protein2Lot = protein2.lot.substring(0, protein2.lot.indexOf(' '));
    }
    var protein1Name = protein1.name || '';
    var protein2Name = protein2.name || '';
    if (protein1.name && protein1.name.indexOf(' ') > 0) {
        protein1Name = protein1.name.substring(0, protein1.name.indexOf(' '));
    }
    if (protein2.name && protein2.name.indexOf(' ') > 0) {
        protein2Name = protein2.name.substring(0, protein2.name.indexOf(' '));
    }
    return protein1Name.toLowerCase() === protein2Name.toLowerCase() &&
        protein1Lot.toLowerCase() === protein2Lot.toLowerCase();
}

function combineProteinsLists(mainProteins, boxProteins) {
    for (var i = 0; i < boxProteins.length; i++) {
        var boxProtein = boxProteins[i];
        for (var j = 0; j < mainProteins.length; j++) {
            var mainProtein = mainProteins[j];
            if (compareProteins(boxProtein, mainProtein)) {
                if (!mainProtein.concentration) {
                    mainProtein.concentration = boxProtein.concentration;
                }
                mainProtein.temperature = boxProtein.temperature;
                mainProtein.tubes = mainProtein.tubes.concat(boxProtein.tubes);
                break;
            }
        }
    }
}

function correctBoxTubes(box, proteins) {
    for (var i = 0; i < box.tubes.length; i++) {
        var tube = box.tubes[i];
        if (tube.lot) {
            for (var j = 0; j < proteins.length; j++) {
                var protein = proteins[j];
                if (protein.lot) {
                    var tubeLot = tube.lot;
                    var proteinLot = protein.lot;
                    if (tube.lot.indexOf(' ') > 0) {
                        tubeLot = tube.lot.substring(0, tube.lot.indexOf(' '));
                    }
                    if (protein.lot.indexOf(' ') > 0) {
                        proteinLot = protein.lot.substring(0, protein.lot.indexOf(' '));
                    }
                    if (tubeLot.toLowerCase() === proteinLot.toLowerCase()) {
                        tube.lot = protein.lot;
                    }
                }
            }
        }
    }
}

function getSheetKey(SheetNames, name) {
    if (!name) {
        return null;
    }
    for (var index = 0; index < SheetNames.length; index++) {
        if (SheetNames[index].indexOf(name) !== -1) {
            return SheetNames[index];
        }
    }
    return null;
}

function writeToFile(file, result) {
    jsonfile.writeFile(file, result, { spaces: 2 }, function (err) {
        if (err) console.error(err);
    });
}

function parseExcelAndSaveData(fileName) {
    var workbook = xlsx.readFile(fileName);

    var firstSheetName = workbook.SheetNames[0];
    var firstSheet = workbook.Sheets[firstSheetName];

    var index = 2;
    var value;
    do {
        var address = "A" + index++;
        value = tools.getValue(firstSheet, address);
    } while (value);

    var proteins = proteinsParser.parse(firstSheet, 2, index - 2);

    var boxesToParse = [];

    for (var index = 0; index < proteins.length; index++) {
        var protein = proteins[index];
        var proteinBoxes = protein.box.split('|');
        for (var b = 0; b < proteinBoxes.length; b++) {
            var boxKey = getSheetKey(workbook.SheetNames, proteinBoxes[b]);
            if (boxKey && boxKey !== '-' && boxesToParse.indexOf(boxKey) === -1) {
                boxesToParse.push(boxKey);
            }
        }
    }
    var boxes = [];
    for (var i = 0; i < boxesToParse.length; i++) {
        var result = boxParser.parse(workbook.Sheets[boxesToParse[i]], boxesToParse[i]);
        combineProteinsLists(proteins, result.proteins);
        correctBoxTubes(result.box, proteins);
        boxes.push(result.box);
    }

    return {
        proteins: proteins,
        boxes: boxes
    }
}

exports.parse = parseExcelAndSaveData;