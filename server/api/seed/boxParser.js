var tools = require('./tools.js');

function parsePiece(tube, piece) {
    if (piece.search(/abc/i) !== -1) {
        tube.protein = piece;
    } else if (piece.indexOf('201') !== -1) {
        tube.lot = piece;
    } else {
        var split = piece.split(' ');
        var pos;
        for (var i = 0; i < split.length; i++) {
            var splitPiece = split[i];
            if ((pos = splitPiece.search(/mg\/mL/i)) !== -1) {
                var concentration = splitPiece.substring(0, pos);
                concentration = concentration.replace(',', '.');
                tube.concentration = Number(concentration);
                if(isNaN(tube.concentration)) {
                    tube.concentration = 0;
                }
            } else if ((pos = splitPiece.search(/mL|µL/i)) !== -1) {
                var volume = splitPiece.substring(0, pos);
                volume = volume.replace(',', '.');
                tube.volume = Number(volume);
                if(isNaN(tube.volume)) {
                    tube.volume = 0;
                }
                tube.unit = splitPiece.substring(pos);
            }
        }
    }
}

function parseTube(string) {
    if (!string) {
        return null;
    }
    var pieces = string.split('\n');
    if (pieces.length === 1) {
        pieces = string.split(' ');
    }
    var tube = {};
    for (var i = 0; i < pieces.length; i++) {
        parsePiece(tube, pieces[i]);
    }
    return tube;
}

function compareTubes(tube1, tube2) {
    if (!tube1.lot || !tube2.lot) {
        return false;
    }
    return tube1.protein.toLowerCase() === tube2.protein.toLowerCase() &&
        tube1.lot.toLowerCase() === tube2.lot.toLowerCase() &&
        tube1.unit.toLowerCase() === tube2.unit.toLowerCase() &&
        tube1.concentration === tube2.concentration &&
        tube1.volume === tube2.volume;
}

function compareProteins(tube, protein) {
    if (!tube.lot || !protein.name) {
        return false;
    }
    return tube.protein.toLowerCase() === protein.name.toLowerCase() &&
        tube.lot.toLowerCase() === protein.lot.toLowerCase();
}

function getUniqueTubesList(tubes) {
    var uniqueTubes = [];

    for (var i = 0; i < tubes.length; i++) {
        var tube = tubes[i];
        if (tube === null) {
            continue;
        }
        var existing = false;
        for (var index = 0; index < uniqueTubes.length; index++) {
            var element = uniqueTubes[index];
            if (compareTubes(tube, element)) {
                element.number++;
                existing = true;
                break;
            }
        }
        if (!existing) {
            uniqueTubes.push({
                protein: tube.protein,
                lot: tube.lot,
                unit: tube.unit,
                volume: tube.volume,
                concentration: tube.concentration,
                temperature: tube.temperature,
                number: 1
            });
        }
    }

    return uniqueTubes;
}

function getUniqueProteins(tubes) {
    var uniqueProteins = [];

    for (var i = 0; i < tubes.length; i++) {
        var tube = tubes[i];
        var existing = false;
        for (var index = 0; index < uniqueProteins.length; index++) {
            var element = uniqueProteins[index];
            if (compareProteins(tube, element)) {
                existing = true;
                break;
            }
        }
        if (!existing) {
            uniqueProteins.push({
                name: tube.protein,
                lot: tube.lot
            });
        }
    }

    return uniqueProteins;
}

function combineProteinAndTubes(proteins, tubes) {
    for (var i = 0; i < tubes.length; i++) {
        var tube = tubes[i];
        for (var p = 0; p < proteins.length; p++) {
            var protein = proteins[p];
            if (compareProteins(tube, protein)) {
                if (!protein.tubes) {
                    protein.tubes = [];
                }
                if (!protein.concentration) {
                    protein.concentration = tube.concentration;
                }
                if (!protein.temperature) {
                    protein.temperature = tube.temperature;
                }
                protein.tubes.push({
                    volume: tube.volume,
                    number: tube.number,
                    unit: tube.unit,
                    placed: true
                })
            }
        }
    }
}

function getSimplifiedTube(tube) {
    if (tube === null) {
        return {
            lot: null,
            tube: null
        };
    }
    return {
        lot: tube.lot,
        volume: tube.volume,
        unit: tube.unit,
        protein: tube.protein,
        concentration: tube.concentration
    }
}

function parse(worksheet, worksheetName) {
    var box = {};

    var temperature = null;
    var temperatureStr = tools.getValue(worksheet, 'F5');
    if (temperatureStr) {
        temperatureStr = temperatureStr.replace(' ', '');
        temperatureStr = temperatureStr.replace('°C', '');
        if (temperatureStr) {
            temperature = Number(temperatureStr);
        }
    }

    box.name = tools.getValue(worksheet, 'F6');
    box.project = worksheetName.substring(0, worksheetName.indexOf(' '));
    box.temperature = temperature;
    box.drawer = tools.getValue(worksheet, 'F7');
    box.tubes = [];

    var tubes = [];

    var index = 11;
    var value;
    var size = 0;
    do {
        var address = "A" + index++;
        value = tools.getValue(worksheet, address);
        if (value) {
            size++;
        }
    } while (value);

    box.size = size * size;

    var lineStart = 11;
    var columnStart = 66;
    for (var l = lineStart; l < lineStart + size; l++) {
        for (var c = columnStart; c < columnStart + size; c++) {
            var address = String.fromCharCode(c) + l;
            var tubeStr = tools.getValue(worksheet, address);
            if (tubeStr) {
                tubeStr = tubeStr.replace(/(\r)?/g, '');
            }
            var tube = parseTube(tubeStr);
            if (tube) {
                tube.temperature = box.temperature;
            }
            tubes.push(tube);
            box.tubes.push(getSimplifiedTube(tube));
        }
    }

    var uniqueTubes = getUniqueTubesList(tubes);
    var uniqueProteins = getUniqueProteins(uniqueTubes);

    combineProteinAndTubes(uniqueProteins, uniqueTubes);

    return { box: box, proteins: uniqueProteins };
}

module.exports.parse = parse;