'use strict';

var User = require('./user.model');
var passport = require('passport');
var config = require('../../config/environment');
var jwt = require('jsonwebtoken');
var _ = require('lodash');

var validationError = function (res, err) {
  return res.status(422).json(err);
};

function parseOptions(req) {
  var params = req.query;
  var options = { select: '-salt -hashedPassword' };
  var numericLimit = Number(params.limit);
  if (params.limit && !isNaN(numericLimit)) {
    options.limit = numericLimit;
  }
  if (params.page && params.page.length > 0) {
    options.page = params.page;
  }
  if (params.order && params.order.length > 0) {
    var order = params.order;
    var direction = 1;
    if (order.charAt(0) === '-') {
      order = order.substring(1);
      direction = -1;
    }
    options.sort = {};
    options.sort[order] = direction;
  }
  return options;
}

/**
 * Get list of users
 * restriction: 'admin'
 */
exports.index = function (req, res) {
  User.paginate({ role: { $ne: 'superadmin' } }, parseOptions(req), function (err, users) {
    if (err) return res.send(500, err);
    res.status(200).json(users);
  });
};

/**
 * Creates a new user
 */
exports.create = function (req, res, next) {
  var newUser = new User(req.body);
  newUser.provider = 'local';
  newUser.save(function (err, user) {
    if (err) return validationError(res, err);
    res.status(201).json(user);
  });
};

/**
 * Get a single user
 */
exports.show = function (req, res, next) {
  var userId = req.params.id;

  User.findById(userId, function (err, user) {
    if (err) return next(err);
    if (!user) return res.sendStatus(401);
    res.json(user.profile);
  });
};

/**
 * Deletes a user
 * restriction: 'admin'
 */
exports.destroy = function (req, res) {
  User.findByIdAndRemove(req.params.id, function (err, user) {
    if (err) return res.status(500).send(err);
    return res.sendStatus(204);
  });
};

/**
 * Change a users password
 */
exports.changePassword = function (req, res, next) {
  var userId = req.user._id;
  var oldPass = String(req.body.oldPassword);
  var newPass = String(req.body.newPassword);

  User.findById(userId, function (err, user) {
    if (user.authenticate(oldPass)) {
      user.password = newPass;
      user.save(function (err) {
        if (err) return validationError(res, err);
        res.sendStatus(200);
      });
    } else {
      res.sendStatus(406);
    }
  });
};

/**
 * Get my info
 */
exports.me = function (req, res, next) {
  var userId = req.user._id;
  User.findOne({
    _id: userId
  }, '-salt -hashedPassword', function (err, user) { // don't ever give out the password or salt
    if (err) return next(err);
    if (!user) return res.sendStatus(401);
    res.json(user);
  });
};

/**
 * Authentication callback
 */
exports.authCallback = function (req, res, next) {
  res.redirect('/');
};

/*
* Update user
*/
exports.update = function (req, res, next) {
  if (req.body._id) { delete req.body._id; }
  var userId = req.params.id;

  User.findById(userId, function (err, user) {
    if (err) {
      return res.status(500).send(err);
    }
    if (!user) {
      return res.status(404).send();
    }

    var updated = _.merge(user, req.body);
    updated.save(function (err) {
      if (err) { return res.status(500).send(err); }
      return res.status(201).json(user);
    });
  });
};
