# Proteines Management

## Usage
- Go to http://vigrul-proteins.herokuapp.com
- Login with user:test@example.com / password:test
- Play around with the application

## Roadmap
- Gestion des proteines
    - Lister toutes les protéines
    - Ajouter une nouvelle protéine
    - Voir info d’une protéine
    - Retirer une protéine
    - Archiver les protéines finies
    - Supprimer une protéine
    - Recherche
        - Rapide par nom
        - Avancé par projet, lot, etc.
- Gestion des boites
    - Creation automatique d’une boite lors de la création d’un nouveau projet
    - Remplissage automatique des places dans une boite lors de l’ajout d’une nouvelle protéine
    - Voir boites/positions des tubes d’une protéine
    - Suppression des tubes
    - Suppression des boites (réutilisation)
- Gestion des utilisateurs
    - Plusieurs niveau
        - utilisateur simple
        - utilisateur avancé
        - administrateur
    - Creation d’un nouveau utilisateur
    - Page de gestion des utilisateurs
    - Changement de niveau d’un utilisateur
    - Suppression d’un utilisateur
- Gestion des fichiers
    - Creation automatique des dossier liés aux projets/protéines
    - Upload des PDFs via l’interface web
    - Lecture des fichiers directement dans le navigateur
- Database backup
    - Backup réguliers de la base de données
    - Reinstall rapide d’un backup lors d’un problème
- Installation
- Suivi

## Implementation considerations
### Model
- Project
    - Name
    - Id
- Protein
    - Name
    - Trivial name
    - Project ref
- Batch
    - Name
    - Project ref
    - Concentration
    - Total initial volume
    - Initial quantity
    - Initial number of tubes
    - Concentration
    - Concentration
- Box
- Reference file
- History

## Configuration

## Installation steps

## Planning