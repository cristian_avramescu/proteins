(function () {

  'use strict';

  angular
    .module('ProteinesApp')
    .service('ProteinSvc', ProteinSvc);

  /* @ngInject */
  function ProteinSvc($resource, $filter, $state, $mdToast, $http, Auth) {
    var proteinsUrl = 'api/proteins/';
    var proteinsResource = $resource(proteinsUrl + ':id', { id: "@_id" });

    function formatNumber(value, precision) {
      if (typeof precision === "undefined") {
        precision = 2;
      }
      var numberValue = Number(value);
      var finalValue = -1;
      if (!isNaN(numberValue) && numberValue !== -1) {
        finalValue = parseFloat(Math.round(value * 100) / 100);
      }
      return finalValue.toFixed(precision).replace('.', ',');
    }

    function formatDate(date) {
      return $filter('date')(date, 'dd/MM/yyyy');
    }

    var baseProtein = {
      formattedCreationDate: function () {
        return formatDate(this.creationDate);
      },
      formattedFinishedDate: function () {
        if (this.finishedDate) {
          return formatDate(this.finishedDate);
        }
        return null;
      },
      updateValues: function () {
        var tubesNb = 0;
        var volume = 0;
        var qty = -1;

        if (this.tubes.length) {
          for (var i = 0; i < this.tubes.length; i++) {
            var tube = this.tubes[i];
            var tubeVolume = 0;
            if (tube.unit && tube.unit.toLowerCase() === 'ml') {
              tubeVolume = tube.volume;
            } else {
              tubeVolume = tube.volume / 1000;
            }
            volume += tube.number * tubeVolume;
            tubesNb += tube.number;
          }
        }
        if (this.concentration) {
          qty = this.concentration * volume;
        }

        if (!this.box && !this.finishedDate) {
          this.initialTubesNb = formatNumber(tubesNb, 0);
          this.initialVolume = formatNumber(volume);
          this.initialQuantity = formatNumber(qty);
        }
        this.currentTubesNb = formatNumber(tubesNb, 0);
        this.currentVolume = formatNumber(volume);
        this.currentQuantity = formatNumber(qty);
      },
      addHistory: function (message) {
        if (message[0] === 'PropertiesChanged') {
          for (var i = 0; i < this.history.length; i++) {
            var item = this.history[i];
            if (item.message[0] === 'PropertiesChanged' && !item._id) {
              //should add the property changed event only once per save
              return;
            }
          }
        }
        if (message[0] !== 'ProteinCreated') {
          this.modified = true;
        }
        var currentUser = Auth.getCurrentUser();
        if (currentUser) {
          var author = currentUser.name || currentUser.email;
          this.history.push({
            date: new Date(),
            author: author,
            message: message
          });
        }
      }
    };

    function getAll(query) {
      var promise = this.proteins.get(query).$promise;
      promise.then(function (data) {
        if (data && data.docs) {
          for (var i = 0; i < data.docs.length; i++) {
            var doc = data.docs[i];
            angular.extend(doc, baseProtein);
            doc.updateValues();
          }
        }
      });
      return promise;
    }

    function loadProtein(id, scope) {
      if (id === 'new') {
        this.currentProtein.isNew = true;
        this.currentProtein.tubes = [];
        this.currentProtein.history = [];
        this.currentProtein.creationDate = new Date();
        angular.extend(this.currentProtein, baseProtein);
        this.currentProtein.updateValues();
        this.currentProtein.addHistory(['ProteinCreated']);
        if (scope) {
          scope.$broadcast('proteinLoaded');
        }
      } else {
        var _this = this;
        this.proteins.get({ id: id }, function (data) {
          angular.extend(_this.currentProtein, baseProtein, data);
          _this.currentProtein.updateValues();
          if (scope) {
            scope.$broadcast('proteinLoaded');
          }
        }, function (err) {
          $mdToast.show(
            $mdToast.simple()
              .textContent("La protéine n'éxiste pas")
              .position("top right")
              .hideDelay(3000)
          );
          $state.go('main');
        });
      }
    }

    function getFileUrl(proteinId, fileId) {
      return proteinsUrl + proteinId + '/file/' + fileId + '?cses=' + new Date().getTime();
    }

    function updateProtein(protein) {
      angular.merge(this.currentProtein, protein);
    }

    function deleteFile(proteinId, fileId) {
      return $http.delete('api/proteins/' + proteinId + '/file/' + fileId);
    }

    return {
      proteins: proteinsResource,
      currentProtein: {},
      getAll: getAll,
      loadProtein: loadProtein,
      getFileUrl: getFileUrl,
      updateProtein: updateProtein,
      deleteFile: deleteFile
    };
  }

})();