(function () {

  'use strict';

  angular
    .module('ProteinesApp')
    .service('BoxSvc', BoxSvc);

  /* @ngInject */
  function BoxSvc($resource, $location) {
    this.box = $resource('api/box/:id', { id: "@_id" });

    function isBoxEmpty(box) {
      for (var i = 0; i < box.tubes.length; i++) {
        var tube = box.tubes[i];
        if (tube.lot && tube.protein) {
          return false;
        }
      }
      return true;
    }

    this.transformTubesArrayInMatrix = function (box) {
      var boxSize = box.size;
      if (!boxSize) {
        boxSize = 64;
      }
      var boxSide = Math.sqrt(boxSize);
      var tubesMatrix = [];

      for (var i = 0; i < (boxSide + 1); i++) {
        tubesMatrix[i] = [];
      }

      var matrixIndex = 0;
      var line = tubesMatrix[matrixIndex];
      line.push({});
      for (var i = 1; i < (boxSide + 1); i++) {
        line.push({
          header: i
        });
      }

      for (var i = 0; i < box.tubes.length; i++) {
        var tube = box.tubes[i];
        if (i % boxSide === 0) {
          matrixIndex++;
          line = tubesMatrix[matrixIndex];
          line.push({
            header: String.fromCharCode(65 + matrixIndex - 1)
          });
        }
        line.push(tube);
      }

      return tubesMatrix;
    };

    this.getTransformedBoxes = function (boxList) {
      var result = [];
      for (var i = 0; i < boxList.length; i++) {
        var box = boxList[i];
        if (!isBoxEmpty(box)) {
          box.tubesMatrix = this.transformTubesArrayInMatrix(box);
          result.push(box);
        }
      }
      return result;
    };
  }

})();