(function(){

  'use strict';

  angular
    .module('ProteinesApp')
    .factory('UsersSvc', UsersSvc);

  /* @ngInject */
  function UsersSvc($resource) {
    return $resource('api/users/:id/:controller', {
        id: '@_id'
      },
      {
        changePassword: {
          method: 'PUT',
          params: {
            controller:'password'
          }
        },
        get: {
          method: 'GET',
          params: {
            id:'me'
          }
        },
        getAll: {
          method: 'GET'
        }
      });
  }

})();