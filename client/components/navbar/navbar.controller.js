(function () {

    'use strict';

    angular
        .module('ProteinesApp')
        .controller('NavbarCtrl', NavbarCtrl);

    /* @ngInject */
    function NavbarCtrl($scope, $rootScope, $state, $mdDialog, ProteinSvc, Auth) {
        if (!$scope.page) {
            $scope.page = 'home';
        }

        if ($scope.page === 'protein') {
            $scope.protein = ProteinSvc.currentProtein;
        }
        
        $scope.broadcast = function(event) {
            $rootScope.$broadcast(event);
        }

        $scope.print = function () {
            window.print();
        };

        this.openMenu = function ($mdOpenMenu, ev) {
            originatorEv = ev;
            $mdOpenMenu(ev);
        };

        $scope.currentUser = $scope.getCurrentUser();

        $scope.showUserDialog = function(ev) {
            $mdDialog.show({
                templateUrl: 'components/dialogs/user/dialog.html',
                controller: 'UserDialogCtrl',
                clickOutsideToClose: true,
                targetEvent: ev,
                locals: {
                    user: $scope.currentUser
                }
            }).then(function (refresh) {
                if (refresh) {
                    $scope.showToast("Les informations ont été mises à jour");
                    Auth.refreshCurrentUser(function() {
                       $scope.currentUser = $scope.getCurrentUser();
                       $rootScope.$broadcast('userChanged');
                    });
                }
            });
        }
        
        $scope.showPasswordDialog = function(ev) {
            $mdDialog.show({
                templateUrl: 'components/dialogs/password/dialog.html',
                controller: 'PasswordDialogCtrl',
                clickOutsideToClose: true,
                targetEvent: ev
            }).then(function (refresh) {
                if (refresh) {
                    $scope.showToast("Le mot de passe a été modifié");
                }
            });
        }

        $scope.logout = function () {
            Auth.logout();
            $state.go('login');
        };
    }

})();  