(function () {

    'use strict';

    angular
        .module('ProteinesApp')
        .controller('PasswordDialogCtrl', PasswordDialogCtrl);


    function PasswordDialogCtrl($scope, $mdDialog, Auth, UsersSvc) {
        
        function setMatchValidity(valid) {
            $scope.form.newPassword.$setValidity('match', valid);
            $scope.form.confirmPassword.$setValidity('match', valid);
        }
        
        function clearFormErrors() {
            setMatchValidity(true);
            $scope.form.currentPassword.$setValidity('wrong', true);
        }
        
        $scope.save = function () {
            clearFormErrors();
            var passMatch = $scope.password.new === $scope.password.confirm; 
            setMatchValidity(passMatch);
            $scope.form.$setPristine();
            if ($scope.form.$valid) {
                Auth.changePassword($scope.password.current, $scope.password.new, function(data){
                    if(data.status === 406) {
                        $scope.form.currentPassword.$setValidity('wrong', false);
                    } else {
                        $mdDialog.hide();
                    }
                });
            }
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        }
    }
})();