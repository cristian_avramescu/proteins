(function () {

    'use strict';

    angular
        .module('ProteinesApp')
        .controller('UserDialogCtrl', UserDialogCtrl);


    function UserDialogCtrl($scope, $mdDialog, Auth, UsersSvc, user) {
        if (user !== null) {
            $scope.action = 'edit';
            $scope.user = angular.copy(user);
        } else {
            $scope.action = 'add';
            $scope.user = {role: 'guest', isNew: true};
        }
        $scope.isMe = Auth.getCurrentUser().email === $scope.user.email;
        $scope.password = {};

        $scope.save = function () {
            $scope.form.$setPristine();
            if ($scope.form.$valid) {
                UsersSvc.save($scope.user, function(data){
                    $mdDialog.hide(true);
                }, function(err) {
                    console.log(err);
                });
            }
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        }
    }
})();