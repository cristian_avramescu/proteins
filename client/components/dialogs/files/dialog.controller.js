(function () {

    'use strict';

    angular
        .module('ProteinesApp')
        .controller('FilesDialogCtrl', FilesDialogCtrl);


    function FilesDialogCtrl($scope, $mdDialog, $http, Upload, proteinId) {
        $scope.fileTypes = [
            'Release form',
            'Fiche technique protéine',
            "Certificat d'analyse",
            "Certificat d'analyse (méthodes supplémentaires)",
            'Analyse spectrométrie de masse',
            'Autre'
        ];
        $scope.fileType = $scope.fileTypes[0];


        $scope.upload = function () {
            $scope.progress = 0;
            if ($scope.form.$valid) {
                $scope.uploading = true;
                var fileName = $scope.fileType === 'Autre' ? $scope.otherName : $scope.fileType;
                Upload.upload({
                    url: 'api/proteins/' + proteinId + '/file',
                    data: {
                        filename: fileName,
                        file: $scope.file
                    },
                    withCredentials: true
                }).then(function (data) {
                    $scope.uploading = false;
                    $mdDialog.hide(data);
                }, function (err) {
                    $scope.uploading = false;
                    console.log(err);
                }, function (evt) {
                    $scope.progress = parseInt(100.0 * evt.loaded / evt.total);
                });
            }
        };

        $scope.clear = function () {
            $scope.file = null;
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };
    }
})();