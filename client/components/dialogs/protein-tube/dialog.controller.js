(function () {

    'use strict';

    angular
        .module('ProteinesApp')
        .controller('ProteinTubesDialogCtrl', ProteinTubesDialogCtrl);


    function ProteinTubesDialogCtrl($scope, $mdDialog, tube, action) {
        if (tube !== null) {
            $scope.tube = angular.copy(tube);
        } else {
            $scope.tube = {
                unit: 'µL',
                placed: false,
                unsaved: true
            };
        }
        if (action === 'remove') {
            $scope.tube.toRemove = 1;
        }
        $scope.action = action;

        $scope.save = function () {
            $scope.form.$setPristine();
            $scope.form.$setSubmitted();
            if ($scope.form.$valid) {
                $mdDialog.hide($scope.tube);
            }
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        }
    }
})();