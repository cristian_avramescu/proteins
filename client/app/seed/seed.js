(function () {

  'use strict';

  angular
    .module('ProteinesApp')
    .config(Config);

  /* @ngInject */
  function Config($stateProvider) {
    $stateProvider
      .state('seed', {
        url: '/seed',
        templateUrl: 'app/seed/seed.html',
        authenticate: true,
        controller: 'SeedCtrl'
      });
  }
  
})();