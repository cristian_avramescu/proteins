(function () {

  'use strict';

  angular
    .module('ProteinesApp')
    .controller('SeedCtrl', SeedCtrl);

  function SeedCtrl($scope, Upload) {
    $scope.upload = function () {
      $scope.progress = 0;
      $scope.ok = false;
      if ($scope.form.$valid) {
        $scope.uploading = true;
        Upload.upload({
          url: 'api/seed/',
          data: {
            file: $scope.file
          },
          withCredentials: true
        }).then(function (result) {
          $scope.uploading = false;
          $scope.ok = true;
          $scope.nbProteins = result.data.proteins;
          $scope.nbBoxes = result.data.boxes;
        }, function (err) {
          $scope.uploading = false;
          console.log(err);
        }, function (evt) {
          $scope.progress = parseInt(100.0 * evt.loaded / evt.total);
        });
      }
    };
  }

})();
