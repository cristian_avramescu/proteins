(function () {

    'use strict';

    angular
        .module('ProteinesApp')
        .controller('AppCtrl', AppCtrl);

    function AppCtrl($scope, $mdToast, Auth) {
        $scope.$on('notAuthorized', function () {
            $mdToast.show(
                $mdToast.simple()
                    .textContent("Vous n'êtes pas autorisé à exécuter cette action")
                    .position("top right")
                    .hideDelay(3000)
            );
        });

        $scope.showToast = function(message) {
            $mdToast.show(
                $mdToast.simple()
                    .textContent(message)
                    .position("top right")
                    .hideDelay(3000)
            );
        };

        $scope.hasRole = Auth.hasRole;
        $scope.getCurrentUser = Auth.getCurrentUser;
    }

})();