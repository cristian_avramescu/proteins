(function () {

    'use strict';

    angular
        .module('ProteinesApp')
        .controller('FilesCtrl', FilesCtrl);

    function FilesCtrl($scope, $mdDialog, $mdToast, $window, ProteinSvc) {
        $scope.uploadFile = function (ev) {
            $mdDialog.show({
                templateUrl: 'components/dialogs/files/dialog.html',
                controller: 'FilesDialogCtrl',
                clickOutsideToClose: true,
                targetEvent: ev,
                locals: {
                    proteinId: $scope.protein._id
                }
            }).then(function (result) {
                ProteinSvc.updateProtein(result.data);
                var lastFile = $scope.protein.files[$scope.protein.files.length - 1];
                $scope.protein.addHistory(['FileUpload', lastFile.name, lastFile.version]);
            });
        };

        $scope.viewFile = function (file) {
            $window.open(ProteinSvc.getFileUrl($scope.protein._id, file._id), '_blank');
        };

        $scope.deleteFile = function (ev, file) {
            var confirm = $mdDialog.confirm()
                .title('Le fichier sera définitivement supprimé.')
                .content('Êtes-vous sûr de vouloir continuer?')
                .targetEvent(ev)
                .ok('Oui')
                .cancel('Non');
            $mdDialog.show(confirm).then(function () {
                ProteinSvc.deleteFile($scope.protein._id, file._id).then(function (response) {
                    ProteinSvc.updateProtein(response.data);
                    $scope.protein.files = response.data.files;
                    $scope.protein.addHistory(['FileDelete', file.name, file.version]);
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent("Le fichier a été supprimé")
                            .position("top right")
                            .hideDelay(3000)
                    );
                });
            });
        };
    }

})();
