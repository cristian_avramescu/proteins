(function () {

    'use strict';

    angular
        .module('ProteinesApp')
        .controller('HistoryCtrl', HistoryCtrl);

    function HistoryCtrl($scope, $filter) {
        $scope.formatDate = function (date) {
            return $filter('date')(date, 'dd/MM/yyyy');
        };

        $scope.formatMessage = function (messages) {
            var message = '';
            switch (messages[0]) {
                case 'ProteinCreated':
                    message = 'A créé la protéine';
                    break;
                case 'PropertiesChanged':
                    message = 'A changé les propriétés de la protéine';
                    break;
                case 'BoxComputed':
                    message = 'A positionné les tubes dans la boite';
                    break;
                case 'TubesAdded':
                    message = 'A ajouté ' + messages[1] + ' tube(s) de ' + messages[2];
                    break;
                case 'TubesRemoved':
                    message = 'A rétiré ' + messages[1] + ' tube(s) de ' + messages[2];
                    break;
                case 'TubesChanged':
                    message = 'A changé les tubes de ' + messages[2];
                    break;
                case 'TubesDeleted':
                    message = 'A supprimer les tubes de ' + messages[2];
                    break;
                case 'FileUpload':
                    message = 'A téléchargé le fichier \'' + messages[1] + '\' V(' + messages[2] + ')';
                    break;
                case 'FileDelete':
                    message = 'A supprimé le fichier \'' + messages[1] + '\' V(' + messages[2] + ')';
                    break;
                default:
                    message = messages[0];
                    break;
            }
            return message;
        };
    }

})();
