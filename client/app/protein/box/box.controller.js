(function () {

  'use strict';

  angular
    .module('ProteinesApp')
    .controller('BoxCtrl', BoxCtrl);

  function BoxCtrl($scope, $http, $filter, BoxSvc) {
    $scope.currentHovering = null;
    $scope.boxList = [];
    $scope.currentBoxIndex;
    $scope.tubesMatrix = [];
    $scope.currentHoveringIndex = null;

    function loadBoxes() {
      BoxSvc.box.get({ name: $scope.protein.box }, function (data) {
        if (data.total) {
          setBoxList(data.list);
          $scope.boxLoaded = true;
        }
      });
    }

    function selectBox(index) {
      $scope.currentBoxIndex = index;
      $scope.box = $scope.boxList[index];
      $scope.tubesMatrix = BoxSvc.transformTubesArrayInMatrix($scope.box);
    }

    function setBoxList(boxList) {
      var boxes = $scope.boxList;
      if (!boxes || !boxes.length) {
        boxes = boxList;
      } else {
        for (var i = 0; i < boxList.length; i++) {
          var newBox = boxList[i];
          if (!newBox._id) {
            boxes.push(newBox);
          } else {
            var found = false;
            for (var j = 0; j < boxes.length; j++) {
              var box = boxes[j];
              if (newBox.name === box.name) {
                angular.extend(box, newBox);
                found = true;
                break;
              }
            }
            if (!found) {
              boxes.push(newBox);
            }
          }
        }
      }
      if (boxes.length) {
        $scope.boxList = $filter('orderBy')(boxes, 'name');
        selectBox(0);
      }
    }

    function isLotInBox(box, protein) {
      for (var i = 0; i < box.tubes.length; i++) {
        var tube = box.tubes[i];
        if (tube.lot === protein.lot) {
          console.log('box contains protein');
          return true;
        }
      }
      return false;
    }

    function getAllMatchingBoxes(tube, proteinLot) {
      var boxes = [];
      for (var i = 0; i < $scope.boxList.length; i++) {
        var box = $scope.boxList[i];
        for (var j = 0; j < box.tubes.length; j++) {
          var boxTube = box.tubes[j];
          if (boxTube.lot === proteinLot &&
            tube.volume === boxTube.volume &&
            tube.unit === boxTube.unit) {
            boxes.push(box);
            break;
          }
        }
      }
      return boxes;
    }

    function getBoxTubesInfos(box, tube, lot) {
      var startIndex = -1;
      var endIndex = -1;
      var boxTube;
      for (var i = 0; i < box.tubes.length; i++) {
        boxTube = box.tubes[i];
        if (boxTube.lot === lot && boxTube.volume === tube.volume) {
          if (startIndex === -1) {
            startIndex = i;
          }
        }
        else {
          if (startIndex !== -1 && endIndex === -1) {
            endIndex = i - 1;
          }
        }
      }
      if (endIndex === -1) {
        endIndex = box.size - 1;
      }
      //find the number of empty slots before the start of the batch
      var allFound = false;
      var emptyBefore = 0;
      var startIndexTemp = startIndex - 1;
      while (!allFound && startIndexTemp >= 0) {
        boxTube = box.tubes[startIndexTemp--];
        if (boxTube.lot === null) {
          emptyBefore++;
        } else {
          allFound = true;
        }
      }
      //find the number of empty slots after the end of the batch
      allFound = false;
      var emptyAfter = 0;
      var endIndexTemp = endIndex + 1;
      while (!allFound && endIndexTemp < box.size) {
        boxTube = box.tubes[endIndexTemp++];
        if (boxTube.lot === null) {
          emptyAfter++;
        } else {
          allFound = true;
        }
      }
      return {
        start: startIndex,
        end: endIndex,
        spaceBefore: emptyBefore,
        spaceAfter: emptyAfter,
        box: box
      };
    }

    function getBestBoxToRemoveFrom(boxes) {
      var bestIndex = 0;
      var max = 0;
      for (var i = 0; i < boxes.length; i++) {
        var boxInfo = boxes[i];
        if (boxInfo.spaceBefore > max) {
          bestIndex = i;
          max = boxInfo.spaceBefore;
        }
        if (boxInfo.spaceAfter > max) {
          bestIndex = i;
          max = boxInfo.spaceAfter;
        }
      }
      return boxes[bestIndex];
    }

    function getStartingPoint(boxInfo) {
      var result = {
        startIndex: boxInfo.end,
        direction: -1
      };
      //pick to start from the beginning or the end so that we maximise the free spaces
      if (boxInfo.spaceBefore > boxInfo.spaceAfter) {
        result.startIndex = boxInfo.start;
        result.direction = 1;
      }
      return result;
    }

    $scope.$on('onTubeSelected', function (ev, tube, selected) {
      if (selected) {
        $scope.currentHovering = tube;
      } else {
        $scope.currentHovering = null;
      }
    });

    $scope.$on('tubesRemoved', function (ev, tube, numberOfTubes) {
      var matchingBoxes = getAllMatchingBoxes(tube, $scope.protein.lot);
      var boxesPositions = [];
      for (var i = 0; i < matchingBoxes.length; i++) {
        boxesPositions.push(getBoxTubesInfos(matchingBoxes[i], tube, $scope.protein.lot));
      }
      while (numberOfTubes) {
        var boxPositions = getBestBoxToRemoveFrom(boxesPositions);
        var availableTubes = boxPositions.end - boxPositions.start + 1;
        var tubesToRemove = Math.min(availableTubes, numberOfTubes);
        var startingPoint = getStartingPoint(boxPositions);
        var index = startingPoint.startIndex;
        var box = boxPositions.box;
        numberOfTubes -= tubesToRemove;
        while (tubesToRemove) {
          var boxTube = box.tubes[index];
          if (tubesToRemove > 0 && boxTube.lot === $scope.protein.lot && boxTube.volume === tube.volume) {
            boxTube.removed = true;
            boxTube.lot = null;
            boxTube.volume = null;
            boxTube.protein = null;
            boxTube.concentration = null;
            tubesToRemove--;
          }
          index += startingPoint.direction;
        }
        if (!isLotInBox(box, $scope.protein)) {
          var proteinBoxes = $scope.protein.box.split('|');
          proteinBoxes.splice(proteinBoxes.indexOf(box.name), 1);
          $scope.protein.box = proteinBoxes.join('|');
          $scope.justFinished = true;
        }
        if (numberOfTubes) {
          boxesPositions.splice(boxesPositions.indexOf(boxPositions), 1);
        }
      }
    });

    $scope.$on('proteinSaved', function () {
      if ($scope.protein.box || $scope.protein.keepBox) {
        for (var i = 0; i < $scope.boxList.length; i++) {
          var box = $scope.boxList[i];
          BoxSvc.box.save(box, function (data) {
            loadBoxes();
          }, function (err) {
            console.log(err);
          });
        }
      }
    });

    $scope.$on('proteinLoaded', function () {
      if ($scope.protein.box) {
        loadBoxes();
      } else {
        $scope.boxLoaded = true;
      }
    });

    $scope.setHighlight = function (tube, selected, index) {
      if (tube.lot !== $scope.protein.lot ||
          tube.protein !== $scope.protein.name) {
        return;
      }
      if (selected) {
        $scope.currentHovering = tube;
        $scope.currentHoveringIndex = index;
      } else {
        $scope.currentHovering = null;
        $scope.currentHoveringIndex = null;
      }
      for (var index = 0; index < $scope.protein.tubes.length; index++) {
        var element = $scope.protein.tubes[index];
        if (element.volume === tube.volume && element.unit === tube.unit) {
          element.$selected = selected;
        }
      }
    };

    $scope.computeTubesPositions = function () {
      $http.post('api/proteins/addToBox', $scope.protein).then(function (result) {
        setBoxList(result.data.list);
        var boxNames = [];
        for (var i = 0; i < $scope.boxList.length; i++) {
          var box = $scope.boxList[i];
          boxNames.push(box.name);
        }
        $scope.protein.box = boxNames.join('|');
        for (var t = 0; t < $scope.protein.tubes.length; t++) {
          var tube = $scope.protein.tubes[t];
          if (!tube.placed) {
            tube.placed = true;
            tube.unsaved = true;
          }
        }
        $scope.protein.editMode = false;
        $scope.protein.addHistory(['BoxComputed']);
      });
    };

    $scope.selectBox = function (index) {
      selectBox(index);
    }

    if ($scope.protein && $scope.protein.isNew) {
      $scope.boxLoaded = true;
    }
  }
})();
