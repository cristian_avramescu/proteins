(function () {

    'use strict';

    angular
        .module('ProteinesApp')
        .controller('ProteinCtrl', ProteinCtrl);

    function ProteinCtrl($scope, $rootScope, $stateParams, $state, $mdDialog, $mdToast, ProteinSvc) {
        $scope.page = 'protein';
        $scope.isReadOnly = true;
        var forceLeave = false;
        $scope.protein = null;

        var fieldsToIgnore = ['__v',
            'initialTubesNb',
            'currentTubesNb',
            'initialQuantity',
            'initialVolume',
            'currentQuantity',
            'currentVolume',
            'modified',
            'nameToLower',
            'projectToLower',
            'isNew',
            'files',
            'history',
            'finishedDate',
            'keepBox',
            'editMode',
            '$$hashKey',
            '$selected',
            'placed',
            'unsaved',
            'id'
        ];

        var tubeFieldsToIgnore = ['selected'];

        function isArrayChanged(oldVal, newVal, arrayName) {
            if (oldVal[arrayName].length !== newVal[arrayName].length) {
                return true;
            } else {
                for (var i = 0; i < newVal[arrayName].length; i++) {
                    if (getChangedFields(oldVal[arrayName][i], newVal[arrayName][i]).length) {
                        return true;
                    }
                }
            }
            return false;
        }

        function getChangedFields(oldVal, newVal) {
            var fields = [];
            for (var p in newVal) {
                if (newVal.hasOwnProperty(p)) {
                    if (p === 'tubes') {
                        if(fields.indexOf(p) === -1 && isArrayChanged(oldVal, newVal, p)) {
                            fields.push(p);
                        }
                    } else if (!angular.equals(newVal[p], oldVal[p]) && fieldsToIgnore.indexOf(p) === -1) {
                        fields.push(p);
                    }
                }
            }
            return fields;
        }

        function loadProtein() {
            var proteinId = $stateParams.proteinId;
            if (!proteinId) {
                $state.go('main');
                return;
            }
            if (proteinId === 'new') {
                $scope.mode = 'new';
            } else {
                $scope.mode = 'edit';
            }

            ProteinSvc.loadProtein(proteinId, $scope);

            $scope.protein = ProteinSvc.currentProtein;
        }

        $scope.$watch('protein', function (newVal, oldVal) {
            if (!oldVal || !newVal) {
                return;
            }
            if (typeof oldVal._id !== 'undefined' || newVal.isNew) {
                var changedFields = getChangedFields(oldVal, newVal);
                if (changedFields.length) {
                    $scope.protein.modified = true;
                    if (changedFields.indexOf('tubes') !== -1) {
                        changedFields.splice(changedFields.indexOf('tubes'), 1);
                    }
                    if (changedFields.indexOf('box') !== -1) {
                        changedFields.splice(changedFields.indexOf('box'), 1);
                    }
                    if (changedFields.length) {
                        $scope.protein.addHistory(['PropertiesChanged', changedFields.join(', ')]);
                    }
                    if ($scope.protein && typeof $scope.protein.updateValues === 'function') {
                        $scope.protein.updateValues();
                    }
                }
            }
        }, true);

        $scope.$on('proteinLoaded', function () {
            $scope.loaded = true;
            // $scope.$broadcast('proteinLoaded');
        });

        $scope.$on('actionSave', function () {
            $scope.proteinForm.$setSubmitted();
            if ($scope.proteinForm.$valid) {
                ProteinSvc.proteins.save($scope.protein, function (data) {
                    angular.extend($scope.protein, data);
                    $scope.protein.modified = false;
                    $scope.showToast('La protéine a bien été enregistrée');
                    $scope.protein.isNew = false;
                    $scope.protein.keepBox = false;
                    if ($scope.mode === 'new') {
                        $state.go('protein', { proteinId: $scope.protein._id });
                    }
                    $rootScope.$broadcast('proteinSaved');
                }, function (err) {
                    console.log(err);
                });
            }
        });

        $scope.$on('actionDelete', function () {
            var confirm = $mdDialog.confirm()
                .title('Êtes-vous sûr de vouloir supprimer la protéine?')
                .content('Cette action est irréversible')
                .openFrom('.main-toolbar')
                .closeTo('.main-toolbar')
                .ok('Oui')
                .cancel('Non');
            $mdDialog.show(confirm).then(function () {
                ProteinSvc.proteins.delete({ id: $scope.protein._id }, function (data) {
                    $scope.showToast('La protéine a été supprimée');
                    $state.go('main');
                }, function (err) {
                    console.log(err);
                });
            });
        });

        $scope.$on('actionEdit', function () {
            $scope.protein.editMode = true;
        });

        $scope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
            if ($scope.protein && $scope.protein.modified && !forceLeave) {
                event.preventDefault();
                var confirm = $mdDialog.confirm()
                    .title('Toutes les modification vont être perdues.')
                    .content('Êtes-vous sûr de vouloir continuer?')
                    .openFrom('.main-toolbar')
                    .closeTo('.main-toolbar')
                    .ok('Oui')
                    .cancel('Non');
                $mdDialog.show(confirm).then(function () {
                    forceLeave = true;
                    $state.go(toState, toParams);
                });
            }
            if (fromParams.proteinId && fromParams.proteinId !== 'new' && toParams.proteinId === 'new') {
                event.preventDefault();
                $state.go('main');
            }
            ProteinSvc.currentProtein = {};
        });

        $scope.$on('finishedTubes', function () {
            var alert = $mdDialog.alert()
                .clickOutsideToClose(true)
                .textContent('Quantité de proteine finie.')
                .ariaLabel('Proteine finie')
                .openFrom('.main-toolbar')
                .closeTo('.main-toolbar')
                .ok('OK')
            $mdDialog.show(alert);
            $scope.protein.finishedDate = new Date();
            $scope.protein.keepBox = true;
        });

        loadProtein();
    }

})();
