(function() {

    'use strict';

    angular
        .module('ProteinesApp')
        .config(Config);

    /* @ngInject */
    function Config($stateProvider) {
        $stateProvider
            .state('protein', {
                url: '/protein/:proteinId',
                authenticate: true,
                views: {
                    '@': {
                        templateUrl: 'app/protein/protein.html',
                        controller: 'ProteinCtrl',
                    },
                    'details@protein': {
                        templateUrl: 'app/protein/details/details.html',
                        controller: 'DetailsCtrl'
                    },
                    'tubes@protein': {
                        templateUrl: 'app/protein/tubes/tubes.html',
                        controller: 'TubesCtrl'
                    },
                    'box@protein': {
                        templateUrl: 'app/protein/box/box.html',
                        controller: 'BoxCtrl'
                    },
                    'files@protein': {
                        templateUrl: 'app/protein/files/files.html',
                        controller: 'FilesCtrl'
                    },
                    'history@protein': {
                        templateUrl: 'app/protein/history/history.html',
                        controller: 'HistoryCtrl'
                    }
                }
            });
    }

})();