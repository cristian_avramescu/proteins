(function () {

    'use strict';

    angular
        .module('ProteinesApp')
        .controller('TubesCtrl', TubesCtrl);

    function TubesCtrl($scope, $mdDialog, $injector, $rootScope) {

        $scope.addTube = function (ev) {
            $mdDialog.show({
                templateUrl: 'components/dialogs/protein-tube/dialog.html',
                controller: 'ProteinTubesDialogCtrl',
                clickOutsideToClose: true,
                targetEvent: ev,
                locals: {
                    tube: null,
                    action: 'add'
                }
            }).then(function (newTube) {
                if (newTube) {
                    if (!$scope.protein.tubes) {
                        $scope.protein.tubes = [];
                    }
                    $scope.protein.addHistory(['TubesAdded', newTube.number, newTube.volume + ' ' + newTube.unit]);
                    var added = false;
                    for (var i = 0; i < $scope.protein.tubes.length; i++) {
                        var tube = $scope.protein.tubes[i];
                        if(tube.volume === newTube.volume && tube.unit === newTube.unit) {
                            tube.number += newTube.number;
                            added = true;
                            break;
                        }
                    }
                    if(!added) {
                        $scope.protein.tubes.push(newTube);
                    }
                }
            });
        };

        $scope.modifyTube = function (ev, tube) {
            $mdDialog.show({
                templateUrl: 'components/dialogs/protein-tube/dialog.html',
                controller: 'ProteinTubesDialogCtrl',
                targetEvent: ev,
                clickOutsideToClose: true,
                locals: {
                    tube: tube,
                    action: 'edit'
                }
            }).then(function (resultTube) {
                $scope.protein.addHistory(['TubesChanged', tube.volume + ' ' + tube.unit]);
                angular.extend(tube, resultTube);
            });
        };

        $scope.deleteTube = function (ev, index) {
            var confirm = $mdDialog.confirm()
                .content('Êtes-vous sûr de vouloir supprimer ces tubes?')
                .targetEvent(ev)
                .ok('Oui')
                .cancel('Non');
            $mdDialog.show(confirm).then(function () {
                var tube = $scope.protein.tubes[index];
                $scope.protein.addHistory(['TubesDeleted', tube.volume + ' ' + tube.unit]);
                $scope.protein.tubes.splice(index, 1);
            });
        };

        $scope.removeTubes = function (ev, tube, index) {
            $mdDialog.show({
                templateUrl: 'components/dialogs/protein-tube/dialog.html',
                controller: 'ProteinTubesDialogCtrl',
                targetEvent: ev,
                clickOutsideToClose: true,
                locals: {
                    tube: tube,
                    action: 'remove'
                }
            }).then(function (resultTube) {
                $scope.protein.addHistory(['TubesRemoved', resultTube.toRemove, tube.volume + ' ' + tube.unit]);
                tube.number -= resultTube.toRemove;
                $rootScope.$broadcast('tubesRemoved', tube, resultTube.toRemove);
                if (tube.number === 0) {
                    $scope.protein.tubes.splice(index, 1);
                    if($scope.protein.tubes.length === 0) {
                        $scope.$emit('finishedTubes');
                    }
                }
            });
        };
        
        $scope.setSelected = function(tube, selected) {
            tube.$selected = selected;
            $rootScope.$broadcast('onTubeSelected', tube, selected);
        }
    }

})();
