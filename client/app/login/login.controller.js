(function () {

  'use strict';

  angular
    .module('ProteinesApp')
    .controller('LoginCtrl', LoginCtrl);

  /* @ngInject */
  function LoginCtrl($scope, Auth, $location) {
    $scope.user = {};
    $scope.errors = {};
    $scope.login = login;

    function setLoginMatch(state) {
      $scope.loginForm.username.$setValidity('match', state);
      $scope.loginForm.password.$setValidity('match', state);
    }

    function login(form) {
      setLoginMatch(true)
      form.$setPristine();
      if (form.$valid) {
        Auth.login({
          email: $scope.user.email,
          password: $scope.user.password
        })
          .then(function () {
            // Logged in, redirect to home
            $location.path('/');
          })
          .catch(function (err) {
            setLoginMatch(false);
          });
      }
    };

  }

})();
