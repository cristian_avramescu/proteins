(function () {

  'use strict';

  angular
    .module('ProteinesApp')
    .config(config);

  /* @ngInject */
  function config($stateProvider) {
    $stateProvider
      .state('login', {
        url: '/login',
        templateUrl: 'app/login/login.html',
        controller: 'LoginCtrl'
      });
  }
})();