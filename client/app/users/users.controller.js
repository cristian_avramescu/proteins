(function () {

  'use strict';

  angular
    .module('ProteinesApp')
    .controller('UsersCtrl', UsersCtrl);

  /* @ngInject */
  function UsersCtrl($scope, $http, $mdDialog, UsersSvc) {
    $scope.page = "users";

    var bookmark;
    $scope.query = {
      limit: '10',
      order: 'nameToLower',
      page: 1
    };

    function success(users) {
      $scope.users = users;
    }

    function getUsers(query) {
      $scope.promise = UsersSvc.getAll(query || $scope.query).$promise.then(success);
    }

    function showUserDialog(ev, user) {
      $mdDialog.show({
        templateUrl: 'components/dialogs/user/dialog.html',
        controller: 'UserDialogCtrl',
        clickOutsideToClose: true,
        targetEvent: ev,
        locals: {
          user: user
        }
      }).then(function (refresh) {
        if (refresh) {
          getUsers();
          var message;
          if (user) {
            message = "Les informations de l'utilisateur ont été mises à jour";
          } else {
            message = "L'utilisateur a bine été crée";
          }
          $scope.showToast(message);
        }
      });
    }

    $scope.$on('userChanged', function () {
      getUsers();
    });

    $scope.onPaginate = function (page, limit) {
      getUsers(angular.extend({}, $scope.query, { page: page, limit: limit }));
    };

    $scope.onReorder = function (order) {
      getUsers(angular.extend({}, $scope.query, { order: order }));
    };

    $scope.addItem = function (ev) {
      showUserDialog(ev, null);
    }

    $scope.editUser = function (ev, user) {
      showUserDialog(ev, user)
    };

    $scope.deleteUser = function (user) {
      UsersSvc.remove({ id: user._id }, function (data) {
        $scope.showToast("L'utilisateur '" + user.name + "' a été supprimé");
        getUsers();
      }, function (err) {
        console.log(err);
      });
    };

    $scope.translateRole = function (role) {
      switch (role) {
        case 'guest':
          return 'Invité';
        case 'user':
          return 'Utilisateur';
        case 'admin':
          return 'Administrateur';
        default:
          return role;
      }
    }

    getUsers();
  };

})();