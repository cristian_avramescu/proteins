(function () {

  'use strict';

  angular
    .module('ProteinesApp', [
      'ngCookies',
      'ngResource',
      'ngSanitize',
      'ui.router',
      'ngMaterial',
      'md.data.table',
      'ngMessages',
      'ngFileUpload'
    ])
    .config(config)
    .factory('authInterceptor', authInterceptor)
    .run(run);

  /* @ngInject */
  function config($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider, $mdThemingProvider, $compileProvider) {
    $urlRouterProvider
      .otherwise('/');

    $httpProvider.interceptors.push('authInterceptor');
    
    $httpProvider.defaults.headers.get = $httpProvider.defaults.headers.get || {};
    $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
    $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';
    
    $compileProvider.debugInfoEnabled(false);

    $mdThemingProvider
      .theme('default')
      .primaryPalette('deep-orange')
      .accentPalette('blue');
  }
  
  /* @ngInject */
  function run($rootScope, $state, Auth) {
    // Redirect to login if route requires auth and you're not logged in
    $rootScope.$on('$stateChangeStart', function (event, next) {
      Auth.isLoggedInAsync(function (loggedIn) {
        if (next.name === 'login' && loggedIn) {
          event.preventDefault();
          $state.go('main');
        } else if (next.authenticate && !loggedIn) {
          event.preventDefault();
          $state.go('login');
        }
      });
    });
  }

  /* @ngInject */
  function authInterceptor($rootScope, $q, $cookieStore, $location) {
    return {
      // Add authorization token to headers
      request: function (config) {
        config.headers = config.headers || {};
        if ($cookieStore.get('token')) {
          config.headers.Authorization = 'Bearer ' + $cookieStore.get('token');
        }
        return config;
      },

      // Intercept 401s and redirect you to login
      responseError: function (response) {
        if (response.status === 401) {
          $location.path('/login');
          // remove any stale tokens
          $cookieStore.remove('token');
          return $q.reject(response);
        } else if (response.status === 403) {
          $rootScope.$broadcast('notAuthorized');
          return $q.reject(response);
        }
        else {
          return $q.reject(response);
        }
      }
    };
  }

})();  