(function () {

  'use strict';

  angular
    .module('ProteinesApp')
    .config(Config);

  /* @ngInject */
  function Config($stateProvider) {
    $stateProvider
      .state('boxes', {
        url: '/boxes',
        templateUrl: 'app/boxes/boxes.html',
        authenticate: true,
        controller: 'BoxesCtrl'
      });
  }
  
})();