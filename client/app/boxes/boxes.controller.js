(function () {

  'use strict';

  angular
    .module('ProteinesApp')
    .controller('BoxesCtrl', BoxesCtrl);

  function BoxesCtrl($scope, BoxSvc) {
    $scope.page = 'boxes';
    $scope.loaded = false;

    BoxSvc.box.get(function (data) {
      $scope.boxes = BoxSvc.getTransformedBoxes(data.list);
      $scope.selectedIndex = 0;
      $scope.box = $scope.boxes[0];
      $scope.loaded = true;
    });

    $scope.selectBox = function (index) {
      $scope.selectedIndex = index;
      $scope.box = $scope.boxes[index];
    };
  }

})();
