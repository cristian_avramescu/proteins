(function () {

  'use strict';

  angular
    .module('ProteinesApp')
    .config(config);

  /* @ngInject */
  function config($stateProvider) { 
    $stateProvider
    .state('main', {
      url: '/',
      templateUrl: 'app/main/main.html',
      authenticate: true,
      controller: 'MainCtrl'
    });
  }

})();  