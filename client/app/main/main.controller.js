(function() {

    'use strict';

    angular
        .module('ProteinesApp')
        .controller('MainCtrl', MainCtrl);

    /* @ngInject */
    function MainCtrl($scope, $http, $state, $mdDialog, $mdToast, ProteinSvc) {
        var bookmark;

        $scope.filter = {
            options: {
                debounce: 500
            }
        };

        $scope.query = {
            filter: '',
            limit: '10',
            order: '',
            page: 1
        };

        function success(proteins) {
            $scope.proteins = proteins;
            for (var i = 0; i < $scope.proteins.length; i++) {
                $scope.proteins[i].updateValues();
            }
        }

        function getProteins(query) {
            $scope.promise = ProteinSvc.getAll(query || $scope.query);
            $scope.promise.then(success);
        }

        $scope.onPaginate = function(page, limit) {
            getProteins(angular.extend({}, $scope.query, { page: page, limit: limit }));
        };

        $scope.onReorder = function(order) {
            getProteins(angular.extend({}, $scope.query, { order: order }));
        };

        $scope.addItem = function(ev) {
            $state.go('protein', {proteinId: 'new'});
        };

        $scope.formatTemperature = function(value) {
            if (typeof value !== 'undefined') {
                value += '°';
            }
            return value;
        };
        
        $scope.openProtein = function(id) {
            $state.go('protein', {proteinId: id});
        };
        
        $scope.$watch('query.filter', function(newValue, oldValue) {
            if (!oldValue) {
                bookmark = $scope.query.page;
            }

            if (newValue !== oldValue) {
                $scope.query.page = 1;
            }

            if (!newValue) {
                $scope.query.page = bookmark;
            }

            getProteins();
        });
    }

})();